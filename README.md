# Multi-node Kubernetes cluster with Vagrant and Ansible

This is a guide for anyone wanting to setup multi-node kubernetes cluster.

## Table of contents
- [Multi-node Kubernetes cluster with Vagrant and Ansible](#multi-node-kubernetes-cluster-with-vagrant-and-ansible)
  - [Table of contents](#table-of-contents)
  - [Architecture](#architecture)
  - [Prerequisite](#prerequisite)
    - [Hardware](#hardware)
    - [Operating System:](#operating-system)
    - [Software](#software)
  - [Installation](#installation)
  - [Getting Started](#getting-started)
        - [Clone this github repo:](#clone-this-github-repo)
        - [Spin up Kubernetes cluster](#spin-up-kubernetes-cluster)
      - [Troubleshooting](#troubleshooting)
      - [SSH into ```k8s-master```](#ssh-into-k8s-master)
  - [SSH Config](#ssh-config)
  - [Using SSHFS to Mount Vagrant on Mac](#using-sshfs-to-mount-vagrant-on-mac)
  - [Remote Kubernetes Cluster](#remote-kubernetes-cluster)
      - [Stop Vagrant](#stop-vagrant)
      - [Destroy Vagrant](#destroy-vagrant)

&nbsp;


Architecture
--

We will setup a 3 nodes Kubernetes cluster on Ubuntu 18.04, which contains the components below:

| IP            | Hostname   | Role   | Version | OS                 |
|---------------|------------|--------|---------|--------------------|
| 192.168.50.10 | k8s-master | Master | v1.14.0 | Ubuntu 18.04.1 LTS |
| 192.168.50.11 | k8s-node-1 | Node1  | v1.14.0 | Ubuntu 18.04.1 LTS |
| 192.168.50.12 | k8s-node-2 | Node2  | v1.14.0 | Ubuntu 18.04.1 LTS |
| 192.168.50.13 | k8s-node-3 | Node3  | v1.14.0 | Ubuntu 18.04.1 LTS |

&nbsp;

[top](#table-of-contents)

&nbsp;


Prerequisite
--

### Hardware

Minimum Recommended Hardware Requirements

| Processor | CPU    | RAM   | Storage | Internet |
|-----------|--------|-------|---------|----------|
| 64-bit    | 8-Core | 16 GB | 20 GB   | 3 Mbps   |

 

### Operating System:
- macOS 10.13+



### Software

- [Kubernetes 1.14+](https://github.com/kubernetes/kubernetes/)
- [Ansible 2.0+](https://github.com/ansible/ansible/)
- [Vagrant 2.2+](https://github.com/hashicorp/vagrant/)
- [Virtualbox 6.0+](https://www.virtualbox.org/wiki/Downloads)
- [Homebrew 2.0.6+](https://brew.sh/)


&nbsp;


[top](#table-of-contents)
&nbsp;


Installation
--

The best way to get started on a macOS is to use [Homebrew](https://brew.sh/). If you already have [Homebrew](https://brew.sh/) installed, you can install ```virtualbox```, ```vagrant```, ```vagrant-manager```,  ```ansible``` and ```kubectl``` by running the following command
```
brew update && brew install virtualbox && brew install vagrant && brew install vagrant-manager && brew install ansible && brew install kubectl
```

If you don't already have [Homebrew](https://brew.sh/) installed, I would strongly recommend that you install it! It's incredibly useful for installing software dependencies like OpenSSL, MySQL, Postgres, Redis, SQLite, and more.

You can install it by running the following command:
```
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

[top](#table-of-contents)
&nbsp;



Getting Started
--

##### Clone this github repo:
```
git clone https://gitlab.com/kingsleyvn/k8s-vagrant.git
```

##### Spin up Kubernetes cluster

Navigate to ```k8s-vagrant/``` directory and then run the following command to start the Vagrant:

```bash
vagrant up
```
Now, wait for kubernetes cluster to be ready. This might take a while to complete.

Then, run ansible playbook

```bash
ansible-playbook playbook.yml
```

&nbsp;


#### Troubleshooting

If you get an error like the one below, you might need to remove and re-install all plugins.
```
VirtualBox Guest Additions: Starting.
An error occurred during installation of VirtualBox Guest Additions 6.0.4. Some functionality may not work as intended.
In most cases it is OK that the "Window System drivers" installation failed.
```

Run the following command to remove and re-install all plugins:
```
vagrant plugin uninstall vagrant-vbguest
vagrant destroy -f
vagrant up
vagrant plugin install vagrant-vbguest
```

&nbsp;


#### SSH into ```k8s-master```

You can now ```ssh``` into the **```k8s-master```** by running the following command:
```
$ vagrant ssh k8s-master
```

You can verify the cluster by checking the nodes. Run the following command to list all the connected nodes:
```
vagrant@k8s-master:~$ kubectl get nodes -o wide
```
```
NAME         STATUS   ROLES    AGE   VERSION   INTERNAL-IP     EXTERNAL-IP   OS-IMAGE             
k8s-master   Ready    master   21m   v1.14.0   192.168.50.10   <none>        Ubuntu 18.04.1 LTS   
k8s-node-1   Ready    <none>   18m   v1.14.0   192.168.50.11   <none>        Ubuntu 18.04.1 LTS   
k8s-node-2   Ready    <none>   17m   v1.14.0   192.168.50.12   <none>        Ubuntu 18.04.1 LTS 
k8s-node-3   Ready    <none>   15m   v1.14.0   192.168.50.12   <none>        Ubuntu 18.04.1 LTS 
```

&nbsp;


Check everything worked:

```
vagrant@k8s-master:~$ kubectl get pods -n kube-system
NAME                                 READY   STATUS    RESTARTS   AGE
calico-node-jlxkz                    1/1     Running   0          2m57s
calico-node-ml55w                    1/1     Running   0          6m8s
calico-node-shmsr                    1/1     Running   0          4m29s
coredns-86c58d9df4-8lmjw             1/1     Running   0          6m8s
coredns-86c58d9df4-w5l9m             1/1     Running   0          6m8s
etcd-k8s-master                      1/1     Running   0          5m11s
kube-apiserver-k8s-master            1/1     Running   0          5m28s
kube-controller-manager-k8s-master   1/1     Running   0          5m30s
kube-proxy-8lrsv                     1/1     Running   0          6m8s
kube-proxy-bqw9k                     1/1     Running   0          2m57s
kube-proxy-svdf2                     1/1     Running   0          4m29s
kube-scheduler-k8s-master            1/1     Running   0          5m14s
```

&nbsp;

[top](#table-of-contents)
&nbsp;


SSH Config
--
If you want to ```ssh``` into your vagrant box without switching into the project directory and typing ```vagrant ssh k8s-master```. Simply copy ```vagrant ssh-config``` to your ```~/.ssh/config```

```
$ cd kubernetes.resources/k8s-vagrant
$ vagrant ssh-config >> ~/.ssh/config
```
This will allow you to use ```ssh k8s-master``` from anywhere.

[top](#table-of-contents)
&nbsp;


Using SSHFS to Mount Vagrant on Mac
--
The first step is to install ```osxfuse``` and ```sshfs``` via Homebrew:
```
brew cask install osxfuse
brew install sshfs
```

Now, create a local directory in your ```~/Desktop```
```
$ mkdir ~/Desktop/k8s
```
Mount your remote directory in ```~/Desktop/k8s```
```
$ sshfs vagrant@k8s-master:/home/vagrant ~/Desktop/k8s
```

[top](#table-of-contents)
&nbsp;


Remote Kubernetes Cluster
--

To connect to a remote Kubernetes cluster from you Mac, you'll need copy the Kubernetes credentials to your home directory as shown below.

**This will overwrite your current kubectl config, be careful**

```bash
scp -r vagrant@k8s-master:/home/vagrant/.kube ~/.kube/
```

That’s all you have to do. Your local Kubectl should be able to connect with the remote Kubernetes cluster now.


[top](#table-of-contents)
&nbsp;



#### Stop Vagrant

Stop the vagrant machine now.
```
$ vagrant halt
```

&nbsp;


#### Destroy Vagrant

Stops and deletes all traces of the vagrant machine

```
$ vagrant destroy
```

Other useful commands are ```suspend```, ```destroy``` etc.


&nbsp;

[top](#table-of-contents)


